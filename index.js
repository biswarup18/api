const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const udb = require('./app/models/user')
const pdb = require('./app/models/payment')
const sdb = require('./app/router/user')
const rdb = require('./app/models/record')
const db = require('./app/models/book')
const bdb = require('./app/router/book')

const port = 3000

app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)

app.get('/', (request, response) => {
  response.json({ info: 'Node.js, Express, and Postgres API' })
})


//Record CRUD from models/record.js
  app.get('/records',rdb.getrecord)
  app.get('/records/:id',rdb.getr_id)
  app.post('/records',rdb.createrecord)
  app.put('/records/:id',rdb.updaterecord)
  app.delete('/records/:id',rdb.deleterecord)

//Payment CRUD from model/payment.js
app.get('/payments',pdb.getpayments)
app.get('/payments/:id',pdb.getp_id)
app.post('/payments',pdb.createpayments)
app.put('/payments/:id',pdb.updatepayments)
app.delete('/payments/:id',pdb.deletepayments)

//BOOK Search,Sort,Filter from router/book.js
   app.get('/sbook/:b_title',bdb.getbookByname)
   app.get('/sbook/catagory/:subject',bdb.getbookByCatagory)
   app.get('/sortbybname',bdb.getbook)
   app.get('/sortbycopy',bdb.getbookBycopy)

//USER Search,Sort,Filter from router/user.js
  app.get('/user/:u_name',sdb.getUserByname)
  app.get('/user/catagory/:u_name',sdb.getUserByCatagory)
  app.get('/sortbyname',sdb.getUsers)
  app.get('/sortbyreg',sdb.getUserBySortregdate)

//USER CRUD from model/user.js
app.get('/users', udb.getUsers)
app.get('/users/:id', udb.getUserById)
app.post('/users', udb.createUser)
app.put('/users/:id', udb.updateUser)
app.delete('/users/:id', udb.deleteUser)

//BOOK CRUD from models/book.js
app.get('/book', db.getbook)
app.get('/book/:id', db.getbookById)
app.get('/book/ISBN/:ISBN', db.getbookByISBN)
app.post('/book', db.createbook)
app.put('/book/:id', db.updatebook)
app.delete('/book/:id', db.deletebook)

app.listen(port, () => {
  console.log(`App running on port ${port}.`)
})