const pool = require('../config/conn');


  

const getUserByname = (request, response) => {
    const u_name = String(request.params.u_name)
  
    pool.query('SELECT * FROM users WHERE u_name = $1', [u_name], (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).json(results.rows)
    })
  }

const getUserByCatagory = (request, response) => {
    //const catagory = String(request.params.catagory)
    const u_name = String(request.params.u_name)
  
    pool.query('SELECT * FROM users WHERE catagory = $1', [u_name], (error, results) => {
      if (error) {
        throw error
      }
      response.status(201).json(results.rows)
    })
  }
    const getUsers = (request, response) => {
      pool.query('SELECT * FROM users ORDER BY u_name ASC', (error, results) => {
        if (error) {
          throw error
        }
        response.status(202).json(results.rows)
      })
    }
  const getUserBySortregdate = (request, response) => {  
    pool.query('SELECT * FROM users order by reg_date desc', (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).json(results.rows)
    })
  }
  module.exports={
   getUserByname,
   getUserByCatagory,
   getUsers,
   getUserBySortregdate,
   
  }
