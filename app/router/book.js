const pool = require('../config/conn');

const getbookByname = (request, response) => {
    const b_title = String(request.params.b_title)
    
    pool.query('SELECT * FROM book WHERE b_title = $1', [b_title], (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).json(results.rows)
    })
  }

const getbookByCatagory = (request, response) => {
  const subject = String(request.params.subject)
    
  pool.query('SELECT * FROM book WHERE catagory = $1', [subject], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
  }
    const getbook = (request, response) => {
      pool.query('SELECT * FROM book ORDER BY b_title ASC', (error, results) => {
        if (error) {
          throw error
        }
        response.status(202).json(results.rows)
      })
    }
  const getbookBycopy = (request, response) => {  
    pool.query('select b_title, array_agg(b_id) as id from book group by b_title order by id desc', (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).json(results.rows)
    })
  }
  module.exports={
   getbookByname,
   getbookByCatagory,
   getbook,
   getbookBycopy,
   }
