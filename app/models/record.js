const pool = require('../config/conn');

//Read record
const getrecord = (request, response) => {
  pool.query('SELECT * FROM records ORDER BY r_id ASC', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

//Read record by id
const getr_id = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('SELECT * FROM records WHERE r_id = $1', [id], (error, results) => {
       
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}
  
//Create record
const createrecord = (request, response) => {
  const { b_id,u_id,issue_date,submit_date } = request.body

  pool.query('INSERT INTO records (b_id,u_id,issue_date,submit_date) VALUES ($1,$2,$3,$4)', [b_id,u_id,issue_date,submit_date], (error, results) => {
    if (error) {
      throw error
    }
    response.status(201).send(`Record added with record id: ${u_id}`)
  })
}

//Update record
const updaterecord = (request, response) => {
  const id = parseInt(request.params.id)
  const { b_id,u_id,issue_date,submit_date } = request.body

  pool.query(
    'UPDATE records SET b_id=$1,u_id=$2,issue_date=$3,submit_date =$4 WHERE r_id = $5',
    [b_id,u_id,issue_date,submit_date,id],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send(`Record modified with ID: ${id}`)
    }
  )
}


//Delete record
const deleterecord = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('DELETE FROM records WHERE r_id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    
    response.status(200).send(`Records deleted with ID: ${id}`)
  })
}

module.exports = {
  getrecord,
  getr_id,
  createrecord,
  updaterecord,
  deleterecord,
}