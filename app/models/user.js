const pool = require('../config/conn');

//Read user
const getUsers = (request, response) => {
  pool.query('SELECT * FROM users ORDER BY u_id ASC', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

//Read user by id
const getUserById = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('SELECT * FROM users WHERE u_id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

//Create user
const createUser = (request, response) => {
  const { u_name,reg_date,catagory,gender,email } = request.body

  pool.query('INSERT INTO users (u_name,reg_date,catagory,gender,email) VALUES ($1, $2,$3,$4,$5)', [u_name,reg_date,catagory,gender,email ], (error, results) => {
    if (error) {
      throw error
    }
    response.status(201).send(`User added with Name: ${u_name}`)
  })
}

//Update user
const updateUser = (request, response) => {
  const id = parseInt(request.params.id)
  const { u_name,reg_date,catagory,gender,email } = request.body

  pool.query(
    'UPDATE users SET u_name=$1,reg_date=$2,catagory=$3,gender=$4,email=$5 WHERE u_id = $6',
    [u_name,reg_date,catagory,gender,email, id],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send(`User modified with ID: ${id}`)
    }
  )
}


//Delete user
const deleteUser = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('DELETE FROM users WHERE u_id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).send(`User deleted with ID: ${id}`)
  })
}


module.exports = {
  getUsers,
  getUserById,
  createUser,
  updateUser,
  deleteUser,
}