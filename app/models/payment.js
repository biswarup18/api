const pool = require('../config/conn');

//Read payment
const getpayments = (request, response) => {
  pool.query('SELECT * FROM payments ORDER BY p_id ASC', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

//Read payment by id
const getp_id = (request, response) => {
  const id = parseInt(request.params.id)
  pool.query('SELECT * FROM payments WHERE p_id = $1', [id], (error, results) => {
       
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

//Create payment  
const createpayments = (request, response) => {
  const { r_id,fine } = request.body
  pool.query('INSERT INTO payments (r_id,fine) VALUES ($1,$2)', [r_id,fine], (error, results) => {
    if (error) {
      throw error
    }
    response.status(201).send(`Payment added with record id: ${r_id}`)
  })
}

//Update payment
const updatepayments = (request, response) => {
  const id = parseInt(request.params.id)
  const { r_id,fine } = request.body
  pool.query('UPDATE payments SET r_id=$1,fine=$2 WHERE p_id = $3',
    [r_id,fine,id],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send(`Payment modified with ID: ${id}`)
    }
  )
}


//Delete payment
const deletepayments = (request, response) => {
  const id = parseInt(request.params.id)
  pool.query('DELETE FROM payments WHERE p_id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }    
    response.status(200).send(`Payment deleted with ID: ${id}`)
  })
}

module.exports = {
  getpayments,
  getp_id,
  createpayments,
  updatepayments,
  deletepayments,
}