const pool = require('../config/conn');

//Read book
const getbook = (request, response) => {
  pool.query('SELECT * FROM book ORDER BY b_id ASC', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

//Read book by id
const getbookById = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('SELECT * FROM book WHERE b_id = $1', [id], (error, results) => {
    
    
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

//Read book by unique ISBN 
 const getbookByISBN = (request, response) => {
  const ISBN = parseInt(request.params.ISBN)

  pool.query('SELECT * FROM book WHERE ISBN = $1', [ISBN], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

//Create book
const createbook = (request, response) => {
  const { ISBN,b_title,author,catagory,p_date } = request.body

  pool.query('INSERT INTO book (ISBN,b_title,author,catagory,p_date) VALUES ($1,$2,$3,$4,$5)', [ISBN,b_title,author,catagory,p_date], (error, results) => {
    if (error) {
      throw error
    }
    response.status(201).send(`Book added with Book title: ${b_title}`)
  })
}

//Update book
const updatebook = (request, response) => {
  const id = parseInt(request.params.id)
  const { ISBN,b_title,author,catagory,p_date } = request.body

  pool.query(
    'UPDATE book SET ISBN= $1 , b_title = $2 , author =$3 , catagory =$4 ,p_date=$5 WHERE b_id = $6',
    [ISBN,b_title,author,catagory,p_date,id],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send(`Book modified with ID: ${id}`)
    }
  )
}

//Delete book
const deletebook = (request, response) => {
  const id = parseInt(request.params.id)

  pool.query('DELETE FROM book WHERE b_id = $1', [id], (error, results) => {
    if (error) {
      throw error
    }
    
    response.status(200).send(`Book deleted with ID: ${id}`)
  })
}

module.exports = {
  getbook,
  getbookById,
  getbookByISBN,
  createbook,
  updatebook,
  deletebook,
}