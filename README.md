- Nodejs and PostgreSql 


| header | header |
| ------ | ------ |
| cell | cell |
| cell | cell |CREATE TABLE book(
| header | header |
| ------ | ------ |
| cell | cell |
| cell | cell |    b_id SERIAL PRIMARY KEY,
| header | header |
| ------ | ------ |
| cell | cell |
| cell | cell |    ISBN INT NOT NULL UNIQUE,
| header | header |
| ------ | ------ |
| cell | cell |
| cell | cell |    b_title VARCHAR(40),
| header | header |
| ------ | ------ |
| cell | cell |
| cell | cell |    author VARCHAR(40),
| header | header |
| ------ | ------ |
| cell | cell |
| cell | cell |    catagory VARCHAR(40),
| header | header |
| ------ | ------ |
| cell | cell |
| cell | cell |    p_date DATE NOT NULL,
| header | header |
| ------ | ------ |
| cell | cell |
| cell | cell |);


CREATE TABLE users(
    u_id SERIAL PRIMARY KEY,
    u_name VARCHAR(40),
    reg_date DATE,
    catagory VARCHAR(20),
    gender VARCHAR(10),
    email VARCHAR(40),
)

CREATE TABLE records (
	r_id SERIAL PRIMARY KEY,
    b_id INT NOT NULL,
    u_id INT NOT NULL,
	issue_date date,
	submit_date date,
    FOREIGN KEY (b_id) REFERENCES book(b_id),
    FOREIGN KEY (u_id) REFERENCES users(u_id)
);


CREATE TABLE payments (
	p_id SERIAL PRIMARY KEY,
    r_id INT NOT NULL,
    fine INT NOT NULL,
    FOREIGN KEY (r_id) REFERENCES records(r_id)
);

   - type 'node install' in terminal/console in the source folder where 'package.json' is located
   - type 'node index.js' in terminal/console in the source folder where 'index.js' is located
   - server start on port 3000.
   - (http://localhost:3000/)
